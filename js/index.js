import Time from './time/time-custom-events.js';

// Create a new Time() instance
let halloween = new Time('October 31, 2021');

// Listen for date update events
document.addEventListener('time:update', function (event) {

  // Update going to happen
  console.log('update going to happen', event.detail);

  // If the year is greater than 2021, don't update
  if (event.detail.time.date.getFullYear() > 2021) {
    event.preventDefault();
    console.log('update canceled', event.detail);
    return;
  }

  // Update happened
  console.log('update completed', event.detail);
  console.log(halloween.date);

});

// Adjust the date
let newYear = halloween.addDays(3).addMonths(1).addYears(1);
console.log(newYear);
