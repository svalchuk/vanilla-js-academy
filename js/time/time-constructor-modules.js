/**
 * The Constructor object
 * @param {Array} date A date string, object, or array of arguments
 */
function Constructor(date = []) {
  if (!Array.isArray(date)) {
    date = [date];
  }

  this.date = new Date(...date);
}

/**
 * Get name of the day
 * @returns {String}      Name of the day
 */
Constructor.prototype.getDay = function () {
  let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return days[this.date.getDay()];
}

/**
 * Get name of the month
 * @returns {String}     Name of the month
 */
Constructor.prototype.getMonth = function () {
  let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  return months[this.date.getMonth()];
}

/**
 * Add seconds to a date
 * @param   {Number}  number  Seconds to add
 * @returns {Date}            Date
 */
Constructor.prototype.addSeconds = function (number) {
  this.date.setSeconds(this.date.getSeconds() + number);
  return this;
}

/**
 * Add minutes to a date
 * @param   {Number}  number  Minutes to add
 * @returns {Date}            Date
 */
Constructor.prototype.addMinutes = function (number) {
  this.date.setMinutes(this.date.getMinutes() + number);
  return this;
}

/**
 * Add hours to a date
 * @param   {Number}  number  Hours to add
 * @returns {Date}            Date
 */
Constructor.prototype.addHours = function (number) {
  this.date.setHours(this.date.getHours() + number);
  return this;
}

/**
 * Add days to a date
 * @param   {Number}  number  Days to add
 * @returns {Date}            Date
 */
Constructor.prototype.addDays =function (number) {
  this.date.setDate(this.date.getDate() + number);
  return this;
}

/**
 * Add months to a date
 * @param   {Number} number Months to add
 * @returns {Date}          Date
 */
Constructor.prototype.addMonths = function (number) {
  this.date.setMonth(this.date.getMonth() + number);
  return this;
}

/**
 * Add years to a date
 * @param   {Date}   date   Date
 * @param   {Number} number Years to add
 * @returns {Date}          Date
 */
Constructor.prototype.addYears = function (number) {
  this.date.setFullYear(this.date.getFullYear() + number);
  return this;
}

export default Constructor;
