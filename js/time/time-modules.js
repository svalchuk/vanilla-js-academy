/**
 * Get name of the day
 * @param   {Date}  date  The date
 * @returns {String}      Name of the day
 */
function getDay (date) {
  let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return days[date.getDay()];
}

/**
 * Get name of the month
 * @param   {Date}  date The date
 * @returns {String}     Name of the month
 */
function getMonth (date) {
  let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  return months[date.getMonth()];
}

/**
 * Add seconds to a date
 * @param   {Date}    date    The date
 * @param   {Number}  number  Seconds to add
 * @returns {Date}            Date
 */
function addSeconds (date, number) {
  date.setSeconds(date.getSeconds(date) + number);
}

/**
 * Add minutes to a date
 * @param   {Date}    date    The date
 * @param   {Number}  number  Minutes to add
 * @returns {Date}            Date
 */
function addMinutes (date, number) {
  date.setMinutes(date.getMinutes(date) + number);
}

/**
 * Add hours to a date
 * @param   {Date}    date    The date
 * @param   {Number}  number  Hours to add
 * @returns {Date}            Date
 */
function addHours (date, number) {
  date.setHours(date.getHours(date) + number);
}

/**
 * Add days to a date
 * @param   {Date}    date    The date
 * @param   {Number}  number  Days to add
 * @returns {Date}            Date
 */
function addDays (date, number) {
  date.setDate(date.getDate(date) + number);
}

/**
 * Add months to a date
 * @param   {Date}   date   Date
 * @param   {Number} number Months to add
 * @returns {Date}          Date
 */
function addMonths (date, number) {
  date.setMonth(date.getMonth() + number);
}

/**
 * Add years to a date
 * @param   {Date}   date   Date
 * @param   {Number} number Years to add
 * @returns {Date}          Date
 */
function addYears (date, number) {
  date.setFullYear(date.getFullYear() + number);
}

export { getDay, getMonth, addSeconds, addMinutes, addHours, addDays, addMonths, addYears }
