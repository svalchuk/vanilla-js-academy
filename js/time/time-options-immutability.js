/**
 * The Constructor object
 * @param {Array} date A date string, object, or array of arguments
 */
function Constructor(date = [], options = {}) {
  if (!Array.isArray(date)) {
    date = [date];
  }

  let settings = Object.assign({
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  }, options);

  Object.freeze(settings);

  Object.defineProperties(this, {
    date: {value: new Date(...date)},
    _settings: {value: settings}
  })
}

/**
 * Get name of the day
 * @returns {String}      Name of the day
 */
Constructor.prototype.getDay = function () {
  return this._settings.days[this.date.getDay()];
}

/**
 * Get name of the month
 * @returns {String}     Name of the month
 */
Constructor.prototype.getMonth = function () {
  return this._settings.months[this.date.getMonth()];
}

/**
 * Add seconds to a date
 * @param   {Number}  number  Seconds to add
 * @returns {Date}            Date
 */
Constructor.prototype.addSeconds = function (number) {
  let d = new Date(this.date);
  d.setSeconds(d.getSeconds() + number);
  return new Constructor(d, this._settings);
}

/**
 * Add minutes to a date
 * @param   {Number}  number  Minutes to add
 * @returns {Date}            Date
 */
Constructor.prototype.addMinutes = function (number) {
  let d = new Date(this.date);
  d.setMinutes(d.getMinutes() + number);
  return new Constructor(d, this._settings);
}

/**
 * Add hours to a date
 * @param   {Number}  number  Hours to add
 * @returns {Date}            Date
 */
Constructor.prototype.addHours = function (number) {
  let d = new Date(this.date);
  d.setHours(d.getHours() + number);
  return new Constructor(d, this._settings);
}

/**
 * Add days to a date
 * @param   {Number}  number  Days to add
 * @returns {Date}            Date
 */
Constructor.prototype.addDays =function (number) {
  let d = new Date(this.date);
  d.setDate(d.getDate() + number);
  return new Constructor(d, this._settings);
}

/**
 * Add months to a date
 * @param   {Number} number Months to add
 * @returns {Date}          Date
 */
Constructor.prototype.addMonths = function (number) {
  let d = new Date(this.date);
  d.setMonth(d.getMonth() + number);
  return new Constructor(d, this._settings);
}

/**
 * Add years to a date
 * @param   {Date}   date   Date
 * @param   {Number} number Years to add
 * @returns {Date}          Date
 */
Constructor.prototype.addYears = function (number) {
  let d = new Date(this.date);
  d.setFullYear(d.getFullYear() + number);
  return new Constructor(d, this._settings);
}

export default Constructor;
