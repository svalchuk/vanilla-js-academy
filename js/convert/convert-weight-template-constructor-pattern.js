let Convert = (function() {
  /**
   * The Constructor object
   * @param {Number} weight The starting weight in milligrams
   */
  function Constructor (weight) {
    this.weight = weight;
  }

  /**
   * Get weight in milligrams
   * @return {Number} The weight in milligrams
   */
  Constructor.prototype.inMg = function () {
    return this.weight;
  }

  /**
   * Get weight in grams
   * @return {Number} The weight in grams
   */
  Constructor.prototype.inGrams = function () {
    return this.weight / 1000;
  }

  /**
   * Get weight in kilograms
   * @return {Number} The weight in kilograms
   */
  Constructor.prototype.inKg = function () {
    return this.weight / 1000 / 1000;
  }

  return Constructor;
})();
