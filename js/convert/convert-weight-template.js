let convert = (function() {
  /**
   * Convert Grams to Kilograms
   * @param {Number} value The value in grams
   * @returns              The value in milligrams
   */
  function gramsToKg(value) {
    return parseFloat(value) / 1000;
  }

  /**
   * Convert Grams to Milligrams
   * @param {Number} value 
   * @returns {Number}
   */
  function gramsToMg(value) {
    return parseFloat(value) * 1000;
  }

  /**
   * Convert Kilograms to Grams
   * @param {Number} value 
   * @returns {Number}
   */
  function kgToGrams(value) {
    return parseFloat(value) * 1000;
  }

  /**
   * Convert Kilograms to Milligrams
   * @param {Number} value 
   * @returns {Number}
   */
  function kgToMg(value) {
    return parseFloat(value) * 1000 * 1000;
  }

  /**
   * Convert Milligrams to Grams
   * @param {Number} value 
   * @returns {Number}
   */
  function mgToGrams(value) {
    return parseFloat(value) / 1000;
  }

  /**
   * Convert Milligrams to Kilograms
   * @param {Number} value 
   * @returns {Number}
   */
  function mgToKg(value) {
    return parseFloat(value) / 1000 / 1000;
  }

  return { gramsToKg, gramsToMg, kgToGrams, kgToMg, mgToGrams, mgToKg }
})();
