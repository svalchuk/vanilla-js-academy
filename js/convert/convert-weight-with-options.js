let Convert = (function() {

  function weightToMg(weight, units) {
    if (units === 'grams' ) return weight * 1000;
    if (units === 'kg' ) return weight * 1000 * 1000;
    return weight;
  }

  /**
   * The Constructor object
   * @param {Number} weight The starting weight in milligrams
   * @param {Object} options Settings and options for this instance
   */
  function Constructor (weight, options = {}) {
    let settings = Object.assign({
      units: 'mg'
    }, options);

    // Make sure valid units were provided
    if (!['mg', 'grams', 'kg'].includes((settings.units))) {
      throw `[Converts.js]: "${settings.units}" is not a valid weight`;
    }

    this.weight = weightToMg(weight, settings.units)

    // let chooseWeight = {
    //   'grams': weight * 1000,
    //   'kg': weight * 1000 * 1000,
    //   'default': weight,
    // }

    // this.weight = chooseWeight[settings.units] || chooseWeight['default'];

    // switch(settings.units) {
    //   case 'kg':
    //     this.weight = weight * 1000 * 1000;
    //     break;
    //   case 'grams':
    //     this.weight = weight * 1000;
    //     break;
    //   default:
    //     this.weight = weight;
    // }

    // this.settings = settings;
  }

  /**
   * Add milligrams to the weight
   * @param {Number} weight The weight to add
   * @returns 
   */
  Constructor.prototype.addMg = function (weight) {
    this.weight = this.weight + weight;
    return this;
  }

  /**
   * Add grams to the weight
   * @param {Number} weight The weight to add
   * @returns 
   */
  Constructor.prototype.addGrams = function (weight) {
    this.weight = this.weight + weight * 1000;
    return this;
  }

  /**
   * Add kilograms to the weight
   * @param {Number} weight The weight to add
   * @returns 
   */
  Constructor.prototype.addKg = function (weight) {
    this.weight = this.weight + weight * 1000 * 1000;
    return this;
  }

  /**
   * Get weight in milligrams
   * @return {Number} The weight in milligrams
   */
  Constructor.prototype.inMg = function () {
    return this.weight;
  }

  /**
   * Get weight in grams
   * @return {Number} The weight in grams
   */
  Constructor.prototype.inGrams = function () {
    return this.weight / 1000;
  }

  /**
   * Get weight in kilograms
   * @return {Number} The weight in kilograms
   */
  Constructor.prototype.inKg = function () {
    return this.weight / 1000 / 1000;
  }

  return Constructor;
})();

// Create a new instance in grams
let hummingbird = new Convert(4, {
	units: 'grams'
});

// Create an instance with the default milligrams
let bumbleBee = new Convert(150);

// Try to use an invalid unit
// throws an error: ""
// let dragons = new Convert(5, {
// 	units: 'dragonpounds'
// });

// returns 4
let birdWeight = hummingbird.inGrams();
console.log(birdWeight);

// returns 0.15
let beeWeight = bumbleBee.inGrams();
console.log(beeWeight);
